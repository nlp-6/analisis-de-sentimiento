# Analisis de sentimiento

El proyecto consta de 3 carpetas las cuales estan divididas de la siguiente forma:
- **data** (Contiene el dataset a analizar)
- **models** (Contiene informacion sobre el modelo utilizado)
- **notebooks** (Contiene los notebooks con scripts necesarios para el analisis)

# Descripcion general
**1. data**: en data tenemos nuestro bolson de palabras negativas, palabras positivas. Dentro de data está la careta raw, que es donde tenemos nuestra data cruda de los reviews en un documento llamado "reviews.csv" 

**2. models**: 
El modelo implementado es el de bigramas

**3. notebooks**
- -->predict.ipynb: en este notebook cargamos el model.pkl que se encuentra en la carpeta models para realizar predicciones de las frases devolviendo si es positiva o negativa.
- -->train.ipynb: en este notebook realizamos el split de nuestra data y lo dividimos en entrenamiento, test y validación, y realizamos el diccionario de nuestras palabras positivas y negativas
- -->bag_of_words.ipynb: este contiene nuestra lista de palabras positivas y negativas para formar nuestro diccionario de datos.
- -->prepare_data.ipynb: en este archivo se carga la data con su rating y el review y se genera una gráfica en base al hiperparametro de rating.
- -->tokenize.ipynb: en el tokenize se realizó la tokenización de las pablabras.



